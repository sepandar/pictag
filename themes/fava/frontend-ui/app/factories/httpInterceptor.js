'use strict'

angular
  .module('fava')
  .factory('httpInterceptor', function ($rootScope, $q, $injector, authService, baseUrl){
    return {

      request: function (config){

        if (config.url.indexOf ('.html') > -1)
        {
          config.url = baseUrl + config.url + '?rand=' + Date.now();
          return config;
        }

        var token = authService.getToken();

        if (token != null)
          config.headers.Authorization= 'Bearer ' + token;

        $rootScope.lock_submit = true;
        return config;
      },

      requestError: function (rejectionReason){
        return $q.reject('Could not recover beacuse ', rejectionReason);
      },

      response: function (response){

        $rootScope.lock_submit = false;
        return response;
      },

      responseError: function (data, status, headers, config){

        if (data.status === 503) {

          if (data.config.attempts)
            data.config.attempts ++;
          else
            data.config.attempts = 1;

          if (data.config.attempts < 5)
            return $injector.get('$http')(data.config);
        }

        $rootScope.lock_submit = false;
        return $q.reject(data, status, headers, config);
      }
    };
  });
