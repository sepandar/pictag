'use strict'

angular
  .module ('fava', [
    'ngCookies',
    'ui.router',
    'ui.bootstrap.tpls',
    'ui.bootstrap',
    'ngJsTree',
    'ngAside',
    'cfp.hotkeys'
  ]);

angular
  .module ('fava')
  .run (function ($rootScope, $state, authService){

    $rootScope.checkAccess = function (){

      if (authService.authenticated() == false)
      {
        $state.go ('login');
      }
      else {
        if ($state.current.name == '')
          $state.go ('dashboard');
      }
    };

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){

      if (authService.authenticated() == false && toState.name !== 'login')
        event.preventDefault();
    });

    $rootScope.$on ('$stateChangeSuccess', function(){
      $rootScope.checkAccess();
    });

    $rootScope.$on ('Logout', function (){
      $rootScope.checkAccess();
    });

    $rootScope.checkAccess();
  });

angular
  .module ('fava')
  .config (function ($httpProvider, $provide, hotkeysProvider){

    //hot keys config
    hotkeysProvider.useNgRoute = false;

    // change ui-bootstrap progressbar template url
    $provide.decorator('uibProgressbarDirective', function($delegate) {
      $delegate[0].templateUrl = 'app/views/templates/ui-bootstrap/progressbar/progressbar.html';
      return $delegate;
    });

    $provide.decorator('uibModalWindowDirective', function($delegate) {
      $delegate[0].templateUrl = 'app/views/templates/ui-bootstrap/modal/window.html';
      return $delegate;
    });

    $provide.decorator('uibModalBackdropDirective', function($delegate) {
      $delegate[0].templateUrl = 'app/views/templates/ui-bootstrap/modal/backdrop.html';
      return $delegate;
    });

    $httpProvider.interceptors.push('httpInterceptor');
  });


angular
  .module ('fava')
  .constant ('configuration', {})
