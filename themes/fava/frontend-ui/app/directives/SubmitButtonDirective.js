'use strict'

angular
  .module ('fava')
  .directive ('submitButton', function ($http){
    return {

      restrict: 'AEC',
      replace: true,
      template: '<input type="submit" ng-dblclick="\'return false\'"  value="" ng-disabled="lock_submit" />',
      link: function(scope, elem, attrs) {

        attrs.$set ('value', attrs.title);
      }
    };
  });
