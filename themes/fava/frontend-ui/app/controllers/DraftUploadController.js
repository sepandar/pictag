'use strict'

angular
  .module ('fava')
  .controller ('DraftUploadController', function ($scope, $stateParams, draftService){

    var directories = $stateParams.directories.split (',');

    $scope.done = false;
    $scope.count = directories.length;
    $scope.current = 0;
    $scope.uploading = true;

    $scope.upload = function (id){

      draftService.upload(id)
      .success(function(response){

        if (response.error == true) {
          $('.log ul').append('<li>' + response.message + '</li>');
        }

        $scope.current++;

        if ($scope.current == $scope.count)
        {
          $scope.done = true;
          return false;
        }

        $scope.upload (directories[$scope.current]);
      })
      .error(function(err){
        console.log(err);
      });
    };

    $scope.upload (directories[$scope.current]);
  });
