'use strict'

angular
  .module ('fava')
  .controller ('SidebarController', function ($scope, $aside, authService){

    $scope.open = function(){

      $aside.open({
        templateUrl: 'app/views/menu/aside.html',
        controller: function($scope, $uibModalInstance) {
          $scope.isAdmin = authService.isAdmin();
          $scope.clicked = function (){
            $uibModalInstance.dismiss();
          }
        },
        placement: 'right',
        size: 'sm'
      });
    }
  });
