'use strict'

angular
  .module ('fava')
  .controller ('PaperWhiteController', PaperWhiteController);

function PaperWhiteController ($scope, paperService) {

  $scope.checked = {};
  $scope.papers = new Array();

  paperService.white()
    .success(function(data){
      $scope.papers = data.images;
    });

  $scope.remove = function(id) {

    paperService.remove(id)
      .success(function(){
        angular.element('#img-' + id).remove();
      });
  }

  $scope.removeall = function() {
    var list = _.map($scope.checked, function(id) {
      angular.element('#img-' + id).remove();
      return id;
    })
    .join(',');

    paperService.remove(list, 'batch')
      .success(function(){
        alert('با موفقیت انجام شد');
      });
  }

  $scope.toggleCheck = function(id){

    var border = '';

    if ($scope.checked[id] == null)
    {
      $scope.checked[id] = id;
      border = '2px solid #000';
    }
    else
    {
      delete $scope.checked[id];
      border = '1px solid #ccc';
    }

    angular.element('#img-' + id).css({border: border});
  }

  $scope.restore = function(id) {

    paperService.restore(id)
    .success(function(){
      angular.element('#elm-' + id).remove();
    });
  }
}
