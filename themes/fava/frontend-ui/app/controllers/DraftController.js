'use strict'

angular
  .module ('fava')
  .controller ('DraftController', function ($scope, $state, draftService){

    $scope.locked = true;
    $scope.images = new Array();
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.currentPaper = -1;

    /* Http requests */

    // get list of draft directories
    draftService.list ()
    .success(function(directories){
      $scope.locked = false;
      $scope.treeData = directories;
      $scope.treeConfig.version++;
    });

    $scope.loadDraftById = function (id, offset) {
      var papers = draftService.load (id, offset)
      .success(function(data){
        $scope.locked = false;
        $scope.images = data.images;
        $scope.totalItems = data.total;
      })
    }

    /* watchers */
    $scope.$watch ('locked', function (state){
      if (state == true){
        $scope.images = new Array();
      }
    });

    $scope.upload = function(){
      var selected_nodes = $scope.treeInstance.jstree(true).get_checked(true);

      if (selected_nodes.length == 0)
        return false;

      var nodes = _.chain (selected_nodes)
        .filter (function (node){
          return node.data !== null;
        })
        .map (function (node){
          return node.data.id;
        })
        .value()
        .join(',');

      $state.go ('draft.upload', {
        'directories': nodes
      });
    }

    /* pagination functions */
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function() {
      $scope.locked = true;
      $scope.loadDraftById($scope.currentPaper, ($scope.currentPage * 10) - 10);
    };

    /* tree configurations and methods */
    $scope.treeConfig = {
      core: {
        multiple : true,
        animation: true
      },
      checkbox : {
        whole_node: false,
        tie_selection: false
      },
      plugins: ['types','checkbox'],
      version: 1
    };

    /*
    * Event: get selected node on click
    */
    $scope.onTreeChange = function (){
      var selected = $scope.treeInstance.jstree(true).get_selected(true)[0];

      if (selected.data === null)
        return false;

      $scope.locked = true;
      $scope.currentPaper = selected.data.id;
      $scope.loadDraftById ($scope.currentPaper, 0);
    }

  });
