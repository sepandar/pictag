'use strict'

angular
  .module ('fava')
  .controller ('DocumentArchiveController', function ($scope, documentService, hotkeys){

    $scope.locked = true;
    $scope.images = new Array();
    $scope.documents = new Array();
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.lastPage = 1;
    $scope.currentDocument = -1;
    $scope.currentPaperIndex = -1;
    $scope.editing = false;
    $scope.rotation = 0;
    $scope.zoom = 1;


    $scope.myDocuments = function() {

      $scope.locked = true;

      documentService.my ()
      .success (function(documents){

        $scope.locked = false;
        $scope.documents = _.pluck(documents[0].children, 'text');
        $scope.treeData = documents;
        $scope.treeConfig.version++;
      });
    }

    //load documents
    $scope.myDocuments();

    $scope.loadDocumentById = function (id, offset) {

      $scope.locked = true;

      documentService.load(id, offset)
        .success(function(data){
          $scope.locked = false;
          $scope.images = data.images;
          $scope.currentDocument = id;
          $scope.totalItems = data.total;
          $scope.lastPage = Math.round(data.total / 10);

          if ($scope.editing)
            $scope.openImage(0);
        });
    }

    $scope.requestDocument = function(){
      $scope.locked = true;

      documentService.requestDocument()
      .success(function(documents){

        $scope.locked = false;

        if (documents.status === 'fail')
          return alert(documents.message);

        $scope.treeData = documents;
        $scope.treeConfig.version++;
      });
    }

    $scope.applyDocuments = function(){

      $scope.locked = true;

      documentService.applyDocuments()
      .success(function(documents){
        $scope.myDocuments();
      });
    }

    /* pagination functions */
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function() {
      $scope.locked = true;
      $scope.loadDocumentById($scope.currentDocument, ($scope.currentPage * 10) - 10);
    };

    /* tree configurations and methods */
    $scope.treeConfig = {
      core: {
        multiple : false,
        animation: true
      },
      version: 1
    };

    /*
    * Event: get selected node on click
    */
    $scope.onTreeChange = function (){
      var selected = $scope.treeInstance.jstree(true).get_selected(true)[0];

      if (selected.data === null)
        return false;

      $scope.locked = true;
      $scope.editing = false;
      $scope.currentDocument = selected.data.id;
      $scope.loadDocumentById ($scope.currentDocument, 0);
    }

    /* open image function */
    $scope.openImage = function (index) {
      $scope.zoomreset();
      $scope.editing = true;
      $scope.currentPaperIndex = index;
      $scope.currentPaper = $scope.images[index].id;
      $scope.currentImage = $scope.images[index].url;

      $scope.currentTags = _.map ($scope.images[index].tags, function(tg){
        return (typeof tg === 'object') ? tg.tag_id : tg;
      });
    };

    $scope.cancelEdit = function ()
    {
      $scope.editing = false;
      $scope.currentImage = null;
    }

    $scope.rotateLeft = function(){
      $scope.rotation = ($scope.rotation > -360) ? $scope.rotation - 90 : 0;
      $scope.rotateImage();
    }

    $scope.rotateRight = function(){
      $scope.rotation = ($scope.rotation < 360) ? $scope.rotation + 90 : 0;
      $scope.rotateImage();
    }

    $scope.rotateImage = function(){
      angular.element('#imgedit').css ({
        transform: 'rotate(' + $scope.rotation + 'deg)'
      });
    }

    $scope.$on ('display_next_image', function(data){
      $scope.nextImage();
    });

    $scope.nextImage = function(){

      if ($scope.currentPaperIndex < $scope.images.length - 1)
        $scope.openImage($scope.currentPaperIndex + 1);
      else
      {
        $scope.cancelEdit();

        var nextPage = $scope.currentPage + 1;

        if (nextPage < $scope.lastPage) {
          $scope.editing = true;
          $scope.setPage(nextPage);
          $scope.pageChanged();
        }
        else
          $scope.nextDocument();
      }
    }

    $scope.previousImage = function(){
      if ($scope.currentPaperIndex > 0)
        $scope.openImage($scope.currentPaperIndex - 1);
      else
        $scope.cancelEdit();
    }

    $scope.zoomin = function() {
      $scope.zoom = $scope.zoom + 0.5;

      if ($scope.zoom > 5)
        $scope.zoom = 5;

      angular.element('#imgedit').css({'transform': 'scale(' + $scope.zoom + ')'})
    }

    $scope.zoomout = function() {
      $scope.zoom = $scope.zoom - 0.5;

      if ($scope.zoom < 1)
        $scope.zoom = 1;

      angular.element('#imgedit').css({'transform': 'scale(' + $scope.zoom + ')'})
    }

    $scope.zoomreset = function() {
      $scope.zoom = 1;
      angular.element('#imgedit').css({'transform': 'scale(' + $scope.zoom + ')'})
    }

    $scope.nextDocument = function() {
      var next = $scope.getNextDocument();

      if (next != null)
        $scope.loadDocumentById(next, 0);
    }

    $scope.getNextDocument = function() {
      var cur = $scope.documents.indexOf($scope.currentDocument);
      return $scope.documents[cur + 1] ? $scope.documents[cur + 1] : null;
    }

    hotkeys.bindTo($scope)
    .add('left', 'رفتن به تصویر بعدی', function(e){
      if ($scope.editing) $scope.nextImage();
    })
    .add('right', 'رفتن به تصویر قبلی', function(e){
      if ($scope.editing) $scope.previousImage();
    })
    .add({combo: 'pagedown', description: 'چرخش تصویر در جهت عقربه های ساعت', allowIn: ['INPUT'],
      callback: function(e){
        if ($scope.editing) $scope.rotateRight();
      }
    })
    .add({combo: 'pageup', description: 'چرخش تصویر در خلاف جهت عقربه های ساعت', allowIn: ['INPUT'],
      callback: function(e){
        if ($scope.editing) $scope.rotateLeft();
      }
    })
    .add({combo: '=', description: '',
      callback: function(e){
        if ($scope.editing) $scope.zoomin();
      }
    })
    .add({combo: '-', description: '',
      callback: function(e){
        if ($scope.editing) $scope.zoomout();
      }
    })
    .add({combo: 'esc', description: 'انصراف از ویرایش سند', allowIn: ['INPUT'],
      callback: function(e){
        if ($scope.editing) $scope.cancelEdit();
      }
    })

  });
