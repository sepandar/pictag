'use strict'

angular
  .module ('fava')
  .controller ('UserManagementController', UserManagementController);

function UserManagementController ($scope, $state, $stateParams, userService) {

  $scope.users = new Array();

  userService.list()
  .success(function(users){
    $scope.users = users;
  });

  $scope.useradd = function() {
    $state.go('user.add');
  }

  $scope.save = function (){
    var username = $scope.username;
    var password = $scope.password;
    var role = $scope.role;

    userService.save(username, password, role)
      .success(function(){
        $state.go('user.management');
      })
      .error(function(data, status, headers, config){
        if (status == 409)
          alert('این نام کاربری قبلا گرفته شده است');
      });
  }

  $scope.userEdit = function(id, username) {
    $state.go('user.edit', {username: username});
  }
}
