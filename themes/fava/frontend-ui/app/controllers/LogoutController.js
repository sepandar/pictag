'use strict'

angular
  .module ('fava')
  .controller ('LogoutController', LoginController);

function LoginController ($scope, $state, authService) {

  authService.logout();
}
