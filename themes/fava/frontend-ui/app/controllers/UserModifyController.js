'use strict'

angular
  .module ('fava')
  .controller ('UserModifyController', UserModifyController);

function UserModifyController ($scope, $state, $stateParams, userService) {

  $scope.username = $stateParams.username;

  $scope.edit = function() {
    var password = $scope.password;

    if (password.length < 6)
      return alert('طول رمز حداقل شش کاراکتر باید باشد');

    userService.update($scope.username, password)
    .success(function(){
      alert('تغییر رمز انجام شد');
    });
  }

}
