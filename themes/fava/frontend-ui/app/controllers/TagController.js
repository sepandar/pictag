'use strict'

angular
  .module ('fava')
  .controller ('TagController', function ($scope, paperService, hotkeys){

    $scope.saving = false;

    var data = {
      11: '۱۱ / ترفیع',
      12: '۱۲ / تشویق',
      13: '۱۳ / انتقالات',
      14: '۱۴ / انتصاب',
      15: '۱۵ / رسته بندی',
      16: '۱۶ / ماموریت',
      17: '۱۷ / تحصیلات، آموزشی',
      18: '۱۸ / استخدام',
      19: '۱۹ / اطلاعات فردی',
      20: '۲۰ / پایان خدمت',
      21: '۲۱ / انضباطی',
      22: '۲۲ / عائله',
      23: '۲۳ / مرخصی ها',
      24: '۲۴ / ارزیابی',
    };

    $scope.tagsData = _.map(data, function(name, key){
      return {
        id: 'tg_' + key,
        icon: false,
        text: name,
        data: {id: key}
      }
    });

    /*
    * Event: get checked node on click
    */
    $scope.onTreeChange = function (){
      var tags = _.map($scope.tagsInstance.jstree(true).get_checked(true), function(node){
        return node.data.id;
      });

      angular.element('.fasttag').val(tags.join(' '));
    }

    /* tree configurations and methods */
    $scope.treeTagConfig = {
      core: {
        multiple : true,
        animation: true
      },
      plugins: ['checkbox', 'types'],
      version: 1
    };

    /* change tags tree checked items when changed */
    $scope.$watch ('currentTags', function(tags){

      if (typeof tags === 'undefined')
        return;

      $scope.tagsInstance.jstree(true).uncheck_all(true);

      _.each (tags, function(id){
        $scope.tagsInstance.jstree(true).check_node('tg_' + id);
      });

      angular.element('.fasttag').val(tags.join(' '));
    });

    $scope.tagCheckedChange = function(){

      $scope.tagsInstance.jstree(true).uncheck_all(true);
      _.each(angular.element('.fasttag').val().split(' '), function(id){
        $scope.tagsInstance.jstree(true).check_node('tg_'+id);
      });
    }

    $scope.saveEdit = function (paperType){

      var paperType = paperType || 'doc';
      $scope.saving = true;

      var tags = _.map ($scope.tagsInstance.jstree(true).get_checked(true), function(node){
        return node.data.id;
      });

      // update dom tags
      $scope.images[$scope.currentPaperIndex].tags = tags;

      paperService.save ($scope.currentPaper, tags.join(','), paperType)
        .success(function(){
          $scope.saving = false;
          $scope.tagsInstance.jstree(true).uncheck_all(true);

          // change image
          $scope.$emit ('display_next_image');
        });
    }

    hotkeys.bindTo($scope)
    .add({combo: 'enter enter', description: 'ذخیره سند', allowIn: ['INPUT'],
      callback: function(e){
        $scope.saveEdit();
      }
    })
    .add({combo: 'w w', description: 'برگه سفید', allowIn: ['INPUT'],
      callback: function(e){
        $scope.saveEdit('trash');
      }
    });

  });
