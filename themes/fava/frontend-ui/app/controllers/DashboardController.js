'use strict'

angular
  .module ('fava')
  .controller ('DashboardController', function ($scope, authService){

    $scope.isAdmin = authService.isAdmin();
  });
