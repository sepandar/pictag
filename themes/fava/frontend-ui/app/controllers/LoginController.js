'use strict'

angular
  .module ('fava')
  .controller ('LoginController', LoginController);

function LoginController ($scope, $state, loginService) {

  $scope.login = function () {

    loginService.login ($scope.username, $scope.password)
      .success(function(){
        $state.go ('dashboard');
      })
      .finally (function(){

      });
  }
}
