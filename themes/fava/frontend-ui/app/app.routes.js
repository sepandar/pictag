'use strict'

angular
  .module ('fava')
  .config ( function ($stateProvider, $urlRouterProvider, $locationProvider){

    $stateProvider
      .state ('main', {
        url: '/',
        controller: 'MainController',
        templateUrl: 'app/views/main/index.html'
      })
      .state ('login', {
        url: '/login',
        controller: 'LoginController',
        templateUrl: 'app/views/user/login.html'
      })
      .state ('logout', {
        url: '/logout',
        controller: 'LogoutController'
      })
      .state ('dashboard', {
        url: '/dashboard',
        controller: 'DashboardController',
        templateUrl: 'app/views/user/dashboard.html'
      })

      .state ('draft', {
        abstract: true,
        url: '/draft',
        templateUrl: 'app/views/draft/draft.html'
      })
      .state ('draft.list', {
        url: '/list',
        controller: 'DraftController',
        templateUrl: 'app/views/draft/draft.list.html'
      })
      .state ('draft.upload', {
        url: '/upload/:directories',
        controller: 'DraftUploadController',
        templateUrl: 'app/views/draft/draft.upload.html'
      })
      .state ('document', {
        abstract: true,
        url: '/document',
        templateUrl: 'app/views/document/document.html'
      })
      .state ('document.archive', {
        url: '/archive',
        controller: 'DocumentArchiveController',
        templateUrl: 'app/views/document/document.archive.html'
      })
      .state ('paper', {
        abstract: true,
        url: '/paper',
        templateUrl: 'app/views/paper/paper.html'
      })
      .state ('paper.white', {
        url: '/white',
        controller: 'PaperWhiteController',
        templateUrl: 'app/views/paper/paper.white.html'
      })
      .state ('user', {
        abstract: true,
        url: '/user',
        templateUrl: 'app/views/user/user.html'
      })
      .state ('user.management', {
        url: '/management',
        controller: 'UserManagementController',
        templateUrl: 'app/views/user/user.management.html'
      })
      .state ('user.add', {
        url: '/add',
        controller: 'UserManagementController',
        templateUrl: 'app/views/user/user.add.html'
      })
      .state ('user.edit', {
        url: '/edit/:username',
        controller: 'UserModifyController',
        templateUrl: 'app/views/user/user.edit.html'
      });
});
