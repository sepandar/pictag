'use strict'

angular
  .module ('fava')
  .factory ('paperService', function ($http, $cookies, api){

    return {
      save: function (id, tags, paperType){
        return $http ({
          url: api.url + 'paper/save',
          method: 'GET',
          params: {
            id: id,
            tags: tags,
            paperType: paperType
          }
        });
      },

      white: function() {
        return $http ({
          url: api.url + 'paper/white',
          method: 'GET'
        });
      },

      remove: function (id, batch) {
        return $http ({
          url: api.url + 'paper/remove',
          method: 'GET',
          params: {
            id: id,
            type: batch
          }
        });
      },
      restore: function (id) {
        return $http ({
          url: api.url + 'paper/restore',
          method: 'GET',
          params: {
            id: id
          }
        });
      }
    };
  });
