'use strict'

angular
  .module ('fava')
  .factory ('userService', function ($rootScope, $http, api){

    return {
      list: function() {
        return $http ({
          url: api.url + 'user/list',
          method: 'GET'
        });
      },
      save: function(username, password, role) {
        return $http ({
          url: api.url + 'user/save',
          method: 'POST',
          params: {
            username: username,
            password: password,
            role: role
          }
        });
      },
      update: function(username, password) {
        return $http ({
          url: api.url + 'user/update',
          method: 'POST',
          params: {
            username: username,
            password: password,
          }
        });
      }
    };
  });
