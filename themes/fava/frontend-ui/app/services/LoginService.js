'use strict'

angular
  .module ('fava')
  .factory ('loginService', function ($http, $cookies, api){

    return {
      login: function (username, password){

        return $http ({
          url: api.url + 'user/login',
          method: 'GET',
          params: {
            username: username,
            password: password
          }
        })
        .success (function(user){
          console.log(user);
          $cookies.put ('fava.token', user.token);
          $cookies.put ('fava.role', user.role);
        })
        .error (function (){
          alert('نام کاربری و یا کلمه عبور صحیح نیست');
        });
      }
    };
  });
