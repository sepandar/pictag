'use strict'

angular
  .module ('fava')
  .factory ('authService', function ($rootScope, $cookies){

    return {
      authenticated: function (){
        return (typeof $cookies.get('fava.token') === 'undefined') ? false: true;
      },
      isAdmin: function() {
        return ($cookies.get('fava.role') == 'superadmin') ? true: false;
      },
      getToken: function(){
        var token = $cookies.get('fava.token');
        return (typeof token === 'undefined') ? null: token;
      },
      logout: function (){
        $cookies.remove('fava.token');
        $cookies.remove('fava.role');
        $rootScope.$broadcast ('Logout');
      }
    };
  });
