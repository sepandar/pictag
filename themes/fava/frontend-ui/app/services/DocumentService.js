'use strict'

angular
  .module ('fava')
  .factory ('documentService', function ($http, $cookies, api){

    return {
      load: function (id, offset){
        return $http ({
          url: api.url + 'document/papers',
          method: 'GET',
          params: {
            id: id,
            offset: offset
          }
        });
      },
      my: function (){

        return $http ({
          url: api.url + 'document/my',
          method: 'GET'
        });
      },
      requestDocument: function(){
        return $http ({
          url: api.url + 'document/request',
          method: 'GET'
        });
      },
      applyDocuments: function(){
        return $http ({
          url: api.url + 'document/apply',
          method: 'GET'
        });
      }
    };
  });
