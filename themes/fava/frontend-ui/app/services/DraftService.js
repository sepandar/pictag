'use strict'

angular
  .module ('fava')
  .factory ('draftService', function ($http, $cookies, api){

    return {
      list: function (){

        return $http ({
          url: api.url + 'draft/list',
          method: 'GET'
        });
      },
      load: function (id, offset){
        return $http ({
          url: api.url + 'draft/detail',
          method: 'GET',
          params: {
            id: id,
            offset: offset
          }
        });
      },
      upload: function(id){
        return $http ({
          url: api.url + 'draft/save',
          method: 'GET',
          params: {
            id: id
          }
        });
      }
    };
  });
