'use strict'

var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var mqpacker = require('css-mqpacker');
var csswring = require('csswring');

gulp.task('default', function() {
  // place code for your default task here
});

gulp.task('sass', function () {

  var processors = [
    autoprefixer({browsers: ['last 1 version']}),
    mqpacker
  ];

  gulp.src('public/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(processors))
    //.pipe(sass({outputStyle: 'compressed'}))
    .pipe(gulp.dest('public/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('public/sass/**/*.scss', ['sass']);
});
