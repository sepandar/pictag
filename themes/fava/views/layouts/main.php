<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FontAsset;
use janus\bootstrap\BootstrapRtlAsset;

BootstrapRtlAsset::register($this);
FontAsset::register($this);
$app = AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>" ng-app="fava">

    <head>

        <meta charset="<?= Yii::$app->charset ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>

        <?php $this->beginBody() ?>

            <div class="container">
                <div class="row" ui-view></div>
            </div>

        <?php $this->endBody() ?>

        <script type="text/javascript">
            angular.module ('fava').constant ('baseUrl', '<?= $app->baseUrl; ?>/');
            angular.module ('fava').constant ('api', {'url': '<?= Yii::$app->homeUrl . "api/"; ?>'});
        </script>
    </body>
</html>

<?php $this->endPage() ?>
