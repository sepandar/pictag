<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\modules\api\models\User;

class SiteController extends Controller
{
    public function actionIndex()
    {
        //$user = User::find()->where(['username' => 'admin'])->one();
        //$user->password = '123456';
        //$user->save();

        return $this->render('index');
    }
}
