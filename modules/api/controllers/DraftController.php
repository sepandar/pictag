<?php
namespace app\modules\api\controllers;

use Yii;
use yii\helpers\ArrayHelper;

use app\modules\api\models\Draft;
use app\modules\api\models\Document;
use app\modules\api\models\Paper;
use app\modules\api\models\DocList;
use app\modules\api\controllers\BaseController;
use yii\imagine\Image;

class DraftController extends BaseController {

  /**
   * @return array of behaviors attached to controller
   */
  public function behaviors()
  {
    return ArrayHelper::merge (parent::behaviors(), [

      'verbFilter' => [
        'actions' => [
          'list' => ['GET'],
          'detail' => ['GET']
        ]
      ],
      'authenticator' => []
    ]);
  }

  /**
  * Return list of all drafts
  */
  public function actionList(){

    $drafts = Draft::all();

    // create data array for treeview
    $data = [];

    $data[] = [
      'text' => count ($drafts) . ' پوشه',
      'icon' => 'fa fa-folder-open-o fa-1x',
      'state' => [
        'opened' => true
      ],
      'children' => []
    ];

    foreach ($drafts as $draft) {
      $data[0]['children'][] = [
        'text' => $draft,
        'icon' => 'fa fa-folder-o fa-1x',
        'data' => [
          'id' => $draft
        ]
      ];
    }

    return $data;
  }

  /**
  * Return draft papers by id
  */
  public function actionDetail ($id, $offset){

    $list = [];
    $images = Draft::findById($id);
    $totalCount = count($images);

    //slice array
    $images = array_slice ($images, $offset, 10);

    foreach ($images as $id => $image){
      $img = Image::thumbnail($image['path'], 150, 150);
      $list[] = [
        'id' => $id + 1,
        'data' => base64_encode($img->get('jpg'))
      ];
    }

    return [
      'total' => $totalCount,
      'start' => $offset,
      'end' => $offset + 10,
      'images' => $list
    ];
  }

  /**
  * Save draft to database
  */
  public function actionSave($id){

    if (DocList::isValid($id) == false)
      return ['error' => true, 'message' => 'شماره پرونده ' . $id . ' صحیح نیست'];

    $images = Draft::findById($id);

    //save document
    $document = Document::store ($id);

    if ($document === null)
      return [];

    //save papers of document
    foreach ($images as $image){
      Paper::store ($id, $image['path']);
    }
  }
}
