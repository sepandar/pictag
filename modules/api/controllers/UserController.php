<?php
namespace app\modules\api\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\ConflictHttpException;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

use janus\user\models\ModelMap;
use app\modules\api\models\LoginForm;
use app\modules\api\models\User;
use janus\user\models\Profile;

use app\modules\api\controllers\BaseController;

class UserController extends BaseController {

  /**
   * @return array of behaviors attached to controller
   */
  public function behaviors()
  {
    return ArrayHelper::merge (parent::behaviors(), [

      'verbFilter' => [
        'actions' => [
          'login' => ['GET'],
          'save' => ['POST']
        ]
      ],

      'authenticator' => [
        'except' => ['login', 'register']
      ]

    ]);
  }

  /**
   * login into account and get new token
   * @param $email string user email
   * @param $password string user password
   * @return mixed
   * @throws BadRequestHttpException
   */
  public function actionLogin ($username, $password)
  {
    $model = new LoginForm ();
    $model->username = $username;
    $model->password = $password;

    if ($model->login ())
    {
      return Yii::$app->user->identity->getFields();
    }
    else
    {
      throw new BadRequestHttpException('Invalid Username or Password');
    }
  }

  public function actionList() {

    if (Yii::$app->user->identity->role !== 'superadmin')
      return;

    $users = User::find()
      ->select(['id', 'username', 'role', 'status'])
      ->all();

    return $users;
  }

  public function actionSave($username, $password, $role) {

    if (Yii::$app->user->identity->role !== 'superadmin')
      return;

    if ($username === '' || $password === '' || $role === '')
      return ['status' => 'fail'];

    $username = strtolower(trim($username));

    $user = User::find()
      ->where(['username' => $username])
      ->one();

    if ($user != null)
      throw new ConflictHttpException('Invalid');

    $user = new User;

    $user->username = $username;
    $user->password = $password;
    $user->role = $role;

    if ($user->save(false)) {
      $profile = new Profile;
      $profile->id = $user->primaryKey;
      $profile->first_name = $username;
      $profile->save(false);
    }
  }

  public function actionUpdate($username, $password) {

    if (Yii::$app->user->identity->role !== 'superadmin')
      return;

    if ($username === '' || $password === '')
      return ['status' => 'fail'];

    $username = strtolower(trim($username));

    $user = User::find()
      ->where(['username' => $username])
      ->one();

    if ($user == null)
      throw new NotFoundHttpException('Invalid');

    $user->password = $password;
    $user->save(false);
  }
}
