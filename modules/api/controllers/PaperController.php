<?php
namespace app\modules\api\controllers;

use Yii;
use yii\helpers\ArrayHelper;

use app\modules\api\models\Document;
use app\modules\api\models\Paper;
use app\modules\api\models\Tag;
use app\modules\api\controllers\BaseController;
use yii\imagine\Image;

class PaperController extends BaseController {

  /**
   * @return array of behaviors attached to controller
   */
  public function behaviors()
  {
    return ArrayHelper::merge (parent::behaviors(), [

      'verbFilter' => [
        'actions' => [
          'image' => ['GET'],
        ]
      ],
      'authenticator' => [
        'except' => ['image']
      ]
    ]);
  }

  public function actionImage($id) {

    $paper = Paper::find()
      ->where(['id' => $id])
      ->one();

    if ($paper === null)
      return false;

    header('Content-Type:image/jpeg');
    Yii::$app->response->sendContentAsFile(base64_decode($paper->image), $paper->document_id . '/' . $paper->name, [
      'mimeType' => 'image/jpeg',
      'inline' => true
    ]);
  }

  public function actionSave($id, $tags, $paperType){

      $paper = Paper::find()
      ->where(['id' => $id])
      ->one();

      $paper->type = ( $paperType == 'trash') ? Paper::TYPE_PAPER_TRASH : Paper::TYPE_PAPER_DOC;
      $paper->update(false);

      Tag::sync ($id, $tags);
  }

  public function actionWhite() {

    $list = [];
    $images = Paper::find()
      ->where(['type' => Paper::TYPE_PAPER_TRASH])
      ->all();

    foreach ($images as $key => $image){

      $list[] = [
        'id' => $image->id,
        'url' =>  \yii\helpers\Url::to(['paper/image', 'id' => $image->id]),
        'thumbnail' => $image->thumbnail
      ];
    }

    return [
      'images' => $list
    ];
  }

  public function actionRestore($id) {

    $paper = Paper::find()
      ->where(['id' => $id, 'type' => Paper::TYPE_PAPER_TRASH])
      ->one();

    if ($paper === null)
      return;

    $paper->type = Paper::TYPE_PAPER_DOC;
    $paper->save();
  }

  public function actionRemove($id, $type = null){

    if ($type === 'batch') {

      $list = explode(',', $id);

      foreach ($list as $item) {

        $paper = Paper::find()
          ->where(['id' => $item, 'type' => Paper::TYPE_PAPER_TRASH])
          ->one();

        if ($paper !== null)
          $paper->delete();
      }

    } else {

      $paper = Paper::find()
        ->where(['id' => $id, 'type' => Paper::TYPE_PAPER_TRASH])
        ->one();

      if ($paper === null)
        return;

      $paper->delete();
    }
  }
}
