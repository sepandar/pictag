<?php
namespace app\modules\api\controllers;

use Yii;
use yii\helpers\ArrayHelper;

use app\modules\api\models\Document;
use app\modules\api\models\Paper;
use app\modules\api\controllers\BaseController;
use yii\imagine\Image;

class DocumentController extends BaseController {

  /**
   * @return array of behaviors attached to controller
   */
  public function behaviors()
  {
    return ArrayHelper::merge (parent::behaviors(), [

      'verbFilter' => [
        'actions' => [
          'papers' => ['GET'],
          'my' => ['GET'],
          'request' => ['GET']
        ]
      ],
      'authenticator' => []
    ]);
  }

  /**
  * Return document papers by id
  */
  public function actionPapers ($id, $offset){

    $list = [];
    $images = Paper::findById($id, $offset);

    foreach ($images as $key => $image){

      $list[] = [
        'id' => $image->id,
        'url' =>  \yii\helpers\Url::to(['paper/image', 'id' => $image->id]),
        'thumbnail' => $image->thumbnail,
        'tags' => $image->tags
      ];
    }

    return [
      'total' => Paper::cnt($id),
      'images' => $list
    ];
  }

  /**
  * Return list of all documents assigned to user
  */
  public function actionMy(){

    if (Yii::$app->user->identity->role == 'superadmin')
    {
      $documents = Document::find()->all();
    }
    else
    {
      $documents = Document::findAllByAttributes ([
        'type' => 0,
        'assigned_to' => Yii::$app->user->id
      ]);
    }

    // create data array for treeview
    $data = [];

    $data[] = [
      'text' => count ($documents) . ' پوشه',
      'icon' => 'fa fa-folder-open-o fa-1x',
      'state' => [
        'opened' => true
      ],
      'children' => []
    ];

    foreach ($documents as $document) {
      $data[0]['children'][] = [
        'text' => $document->id,
        'icon' => 'fa fa-folder-o fa-1x',
        'a_attr' => [
          'style' => Document::isDone($document->id) ? 'background: yellow': ''
        ],
        'data' => [
          'id' => $document->id
        ]
      ];
    }

    return $data;
  }

  public function actionApply() {

    $documents = Document::findAllByAttributes ([
      'assigned_to' => Yii::$app->user->id
    ]);

    foreach ($documents as $doc) {

      if (Document::isDone($doc->id) == false)
        continue;

      $doc->type = 10;
      $doc->update();
    }
  }

  public function actionRequest(){

    $userId = Yii::$app->user->id;

    $assigned = Document::find()
      ->where(['assigned_to' => $userId])
      ->all();

    foreach ($assigned as $doc) {
      $papers = Paper::find()
        ->where(['document_id' => $doc->id])
        ->all();

      foreach ($papers as $paper){

        if ($paper->type ===  Paper::TYPE_PAPER_TRASH)
          continue;

        if (count($paper->tags) == 0)
          return ['status' => 'fail', 'message' => 'ابتدا پرونده های قبلی خود را تکمیل کنید'];
      }
    }

    $notAssignedDocument = Document::find()
    ->where(['assigned_to' => null])
    ->one();

    if ($notAssignedDocument !== null){

      $notAssignedDocument->assigned_to = Yii::$app->user->id;
      $notAssignedDocument->save();
    }

    return $this->actionMy();
  }
}
