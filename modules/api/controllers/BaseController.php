<?php
namespace app\modules\api\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use yii\filters\auth\QueryParamAuth;

class BaseController extends Controller
{
	public function behaviors()
	{
	    return array_merge (parent::behaviors(), [

        'contentNegotiator' => [
          'class' => 'yii\filters\ContentNegotiator',
          'formats' => [ 'application/json' => Response::FORMAT_JSON ]
        ],

        'authenticator' => [
  				'class' => \yii\filters\auth\CompositeAuth::className(),
    			'authMethods' => [
    				\yii\filters\auth\HttpBearerAuth::className(),
    				\app\modules\api\components\QueryParamAuth::className(),
    			],
	  		]
	    ]);
	}

}
