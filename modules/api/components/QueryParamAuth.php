<?php

namespace app\modules\api\components;

class QueryParamAuth extends \yii\filters\auth\QueryParamAuth
{
	public $tokenParam = 'token';
}
