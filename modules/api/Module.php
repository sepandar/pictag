<?php
namespace app\modules\api;

use Yii;

class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        /* attach module configuration to application configuration */
        $configurations = require(__DIR__ . '/config/main.php');

        Yii::configure($this, $configurations);

    }
}
