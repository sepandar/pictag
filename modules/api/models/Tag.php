<?php
namespace app\modules\api\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Tag extends ActiveRecord {

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className()
      ]
    ];
  }

  public static function tableName() {
    return 'tag';
  }

  public function rules(){
    return [
      [['paper_id', 'user_id', 'tag_id'], 'required'],
      [['created_at', 'updated_at'], 'default']
    ];
  }

  public static function sync ($paperId, $tags){

    $oldTags = array_keys(Tag::find()
      ->select('tag_id')
      ->indexBy('tag_id')
      ->where(['paper_id' => $paperId ])
      ->asArray()
      ->all());

    $currentTags = explode(',', $tags);

    // tags shoud be inserted in database
    $newTags = array_diff($currentTags, $oldTags);

    // tags should be removed from database
    $removeTags = array_diff($oldTags, $currentTags);

    foreach ($newTags as $tag){
      $model = new Tag();
      $model->paper_id= $paperId;
      $model->tag_id = $tag;
      $model->user_id = Yii::$app->user->id;
      $model->save();
    }

    foreach ($removeTags as $tag){
      $model = Tag::find()
        ->where (['paper_id' => $paperId, 'tag_id' => $tag])
        ->one();

      $model->delete();
    }
  }
}
