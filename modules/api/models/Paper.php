<?php
namespace app\modules\api\models;

use Yii;
use yii\db\ActiveRecord;
use app\modules\api\models\Tag;
use yii\imagine\Image;
use yii\behaviors\TimestampBehavior;

class Paper extends ActiveRecord {

  const TYPE_PAPER_TRASH = 0;
  const TYPE_PAPER_DOC = 1;

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className()
      ]
    ];
  }

  public static function tableName() {
    return 'paper';
  }

  public function rules(){
    return [
      [['document_id', 'name', 'image', 'thumbnail', 'type'], 'required'],
      [['created_at', 'updated_at'], 'default']
    ];
  }

  public function getTags(){
    return $this->hasMany (Tag::className(), ['paper_id' => 'id']);
  }

  public static function cnt ($id){
    return Paper::find()
      ->where(['document_id' => $id])
      ->count();
  }

  public static function findById($id, $offset){
    return Paper::find()
      ->where (['document_id' => $id])
      ->with ([
          'tags' => function($q){
            $q->select(['tag_id', 'paper_id']);
          }
        ])
      ->orderBy('name desc')
      ->offset($offset)
      ->limit(10)
      ->all();
  }

  public static function store ($id, $file){

    if (strtolower(basename ($file)) == 'thumbs.db') {
      return false;
    }

    $paper = Paper::find()
      ->where (['document_id' => $id, 'name' => basename ($file)])
      ->one();

    if ($paper !== null)
      return;

    $paper = new Paper;
    $paper->name = basename ($file);
    $paper->document_id = $id;
    $paper->image = base64_encode(Image::getImagine()->open($file)->get('jpg', ['jpeg_quality' => 80]));
    $paper->thumbnail = base64_encode(Image::thumbnail($file, 150, 150)->get('jpg', ['jpeg_quality' => 80]));
    $paper->type = self::TYPE_PAPER_DOC;

    if ($paper->validate())
    {
      $paper->save();
    }

    unset ($paper);
  }
}
