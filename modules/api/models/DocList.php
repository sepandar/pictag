<?php
namespace app\modules\api\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

class DocList extends ActiveRecord {

  public static function tableName() {
    return 'doclist';
  }

  public static function isValid ($id){
    return DocList::find()
      ->where(['number' => $id])
      ->count() >= 1;
  }
}
