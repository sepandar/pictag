<?php
namespace app\modules\api\models;

use Yii;
use app\modules\api\models\Paper;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

class Document extends ActiveRecord {

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      [
        'class' => TimestampBehavior::className()
      ]
    ];
  }

  public static function tableName() {
    return 'document';
  }

  public function rules(){
    return [
      [['id'], 'required'],
      [['assigned_to', 'created_at', 'updated_at'], 'default']
    ];
  }

  public static function findAllByAttributes ($attrs) {
    return Document::find()
      ->where ($attrs)
      ->orderBy('updated_at')
      ->all();
  }

  public static function store ($id, $assigned_to = null) {

    $doc = Document::find()
      ->where (['id' => $id])
      ->one();

    if ($doc !== null)
      return $doc;

    $doc = new Document();
    $doc->id = $id;
    $doc->assigned_to = $assigned_to;

    if ($doc->validate())
    {
      $doc->save();
      return $doc;
    }

    return null;
  }

  public static function isDone($id) {

    $doc = Document::find()
      ->where(['id' => $id])
      ->one();

    $papers = Paper::find()
      ->where(['document_id' => $doc->id])
      ->all();

    foreach ($papers as $paper) {

      if ($paper->type ===  Paper::TYPE_PAPER_TRASH)
        continue;

      if (count($paper->tags) == 0)
        return false;
    }

    return true;
  }

}
