<?php

namespace app\modules\api\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;

use janus\user\models\User as BaseUser;

class User extends BaseUser
{

	/**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(
            parent::attributes(),
            ['username']
        );
    }

    public function rules ()
    {
    	return array_merge (parent::rules(), [
    		['username', 'required'],
        ['username', 'unique']
    	]);

    }

    public function getFields()
    {
        return [
            'first_name' => $this->profile->first_name,
            'last_name' => $this->profile->last_name,
            'username' => $this->username,
            'token' => $this->auth_key,
            'role' => $this->role
        ];
    }

  /**
   * find user by username
   *
   * @param string $username
   * @param bool $filterActive should display only activated users or all users
   * @return static|null
   */
  public static function findByUsername($username, $filterActive = false)
  {
      $query = static::find()
      ->where('username = :username', [':username' => $username]);

      /* filter active user if requested */
      if ($filterActive == true)
          $query->andWhere(['status' => static::STATUS_ACTIVE]);

      return self::get ('one', $query); //get from cache
  }

	/**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search ($params = [])
    {
        $query = static::find()
            ->with('profile')
            ->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
           'query' => $query
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}
