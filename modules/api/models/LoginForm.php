<?php
namespace app\modules\api\models;

use janus\user\Module;

class LoginForm extends \janus\user\models\UserForm
{
    public $username;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

            $user = User::findByUsername($this->username);
            
            if ($user === null || $user->validatePassword($this->password) == false) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            $user = User::findByUsername($this->username);

            /* save login time */
            $user->touch('login_at');

            return \Yii::$app->user->login($user, 86400);
        } else {
            return false;
        }
    }
}
