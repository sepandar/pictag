<?php

namespace app\modules\api\models;

use Yii;
use yii\helpers\FileHelper;

class Draft {

  public static function getBasePath (){
    return Yii::getAlias('@app') . '/web/draft/';
  }

  public static function all(){

    $list = [];
    $path = self::getBasePath() . '*';

    foreach(glob($path, GLOB_ONLYDIR) as $dir)
      $list[] = basename($dir);

    return $list;
  }

  public static function findById($doc){

    $list = [];
    foreach (FileHelper::findFiles (self::getBasePath() . $doc, [
        'recursive' => false,
      ]) as $dir) {
      $list[] = [
        'path' => $dir,
        'url' => Yii::$app->homeUrl . 'draft/' . $doc . '/' . basename($dir)
      ];
    }
    return $list;
  }
}
