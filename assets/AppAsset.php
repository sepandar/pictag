<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/fava/frontend-ui';

    public $jsOptions = [
        'position' => View::POS_END
    ];

    public $js = [
        'app/app.module.js',
        'app/app.routes.js',

        'app/services/AuthService.js',
        'app/services/LoginService.js',
        'app/services/DraftService.js',
        'app/services/DocumentService.js',
        'app/services/PaperService.js',
        'app/services/UserService.js',

        'app/controllers/MainController.js',
        'app/controllers/SidebarController.js',
        'app/controllers/TagController.js',
        'app/controllers/DashboardController.js',
        'app/controllers/UserManagementController.js',
        'app/controllers/UserModifyController.js',
        'app/controllers/PaperWhiteController.js',
        'app/controllers/LoginController.js',
        'app/controllers/LogoutController.js',

        'app/controllers/DraftController.js',
        'app/controllers/DraftUploadController.js',

        'app/controllers/DocumentArchiveController.js',

        'app/directives/SubmitButtonDirective.js',

        'app/factories/httpInterceptor.js'
    ];

    public $css = [
        'public/css/main.css'
    ];

    public $depends = [
        'app\assets\UnderscoreAsset',
        'app\assets\AngularAsset',
        'app\assets\AngularCookieAsset',
        'app\assets\AngularUiAsset',
        'app\assets\AngularBootstrapAsset',
        'app\assets\AngularUiRouterAsset',
        'app\assets\AngularJsTreeAsset',
        'app\assets\AngularSidebarAsset',
        'app\assets\AngularHotkeyAsset'
    ];
}
