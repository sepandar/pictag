<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularLightboxAsset extends AssetBundle
{
  public $sourcePath = '@bower/angular-bootstrap-lightbox/dist';

  public $js = [
    'angular-bootstrap-lightbox.min.js'
  ];

  public $css = [
    'angular-bootstrap-lightbox.min.css'
  ];
}
