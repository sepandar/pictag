<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularJsTreeAsset extends AssetBundle
{
  public $sourcePath = '@bower/ng-js-tree/dist';

  public $js = [
    'ngJsTree.js'
  ];

  public $depends = [
    'janus\treeview\assets\JsTreeAsset'
  ];
}
