<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularBootstrapAsset extends AssetBundle
{
  public $sourcePath = '@bower/angular-bootstrap/';

  public $js = [
    'ui-bootstrap.min.js',
    'ui-bootstrap-tpls.min.js'
  ];

}
