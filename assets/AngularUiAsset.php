<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularUiAsset extends AssetBundle
{
  public $sourcePath = '@bower/angular-ui/build';

  public $js = [
    'angular-ui.min.js'
  ];

  public $css = [
    'angular-ui.min.css'
  ];
}
