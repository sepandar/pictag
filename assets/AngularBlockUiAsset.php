<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularBlockUiAsset extends AssetBundle
{
  public $sourcePath = '@bower/angular-block-ui/dist';

  public $js = [
    'angular-block-ui.min.js'
  ];

  public $css = [
    'angular-block-ui.min.css'
  ];
}
