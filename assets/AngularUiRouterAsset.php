<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularUiRouterAsset extends AssetBundle
{
  public $sourcePath = '@bower/angular-ui-router/release';

  public $js = [
    'angular-ui-router.min.js'
  ];
}
