<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularHotkeyAsset extends AssetBundle
{
  public $sourcePath = '@bower/angular-hotkeys';

  public $js = [
    'build/hotkeys.min.js'
  ];

  public $css = [
    'build/hotkeys.min.css'
  ];
}
