<?php

namespace app\assets;

use yii\web\AssetBundle;

class FontAsset extends AssetBundle
{

    public $sourcePath = '@bower/components-font-awesome';

    public $css = [
        'css/font-awesome.min.css',
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            return preg_match('%(/|\\\\)(fonts|css)%', $from);
        };
    }
}
