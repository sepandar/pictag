<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularSidebarAsset extends AssetBundle
{
  public $sourcePath = '@bower/angular-aside/dist';

  public $js = [
    'js/angular-aside.min.js'
  ];

  public $css = [
    'css/angular-aside.min.css'
  ];

  public $depends = [

  ];
}
