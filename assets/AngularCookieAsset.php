<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularCookieAsset extends AssetBundle
{
  public $sourcePath = '@bower/angular-cookies';

  public $js = [
    'angular-cookies.min.js'
  ];
}
