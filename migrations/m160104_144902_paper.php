<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_144902_paper extends Migration
{
    public function up()
    {
      $tableOptions = null;

      if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable ('paper', [

        'id' => 'BIGINT(20) PRIMARY KEY AUTO_INCREMENT',
        'document_id' => 'VARCHAR(15)',
        'name' => 'VARCHAR(60)',
        'image' => 'LONGBLOB NOT NULL',
        'thumbnail' => 'BLOB NOT NULL',
        'created_at' => 'INTEGER',
        'updated_at' => 'INTEGER',
      ],  $tableOptions);

      //index topic_id attribute, the fourth argument means the topic)id field is not unique
      $this->createIndex('paper_docid', 'paper', 'document_id', false);

      $this->addForeignKey ( 'fk_document_paper_id', 'paper', 'document_id', 'document', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
      $this->dropTable ('paper');
    }
}
