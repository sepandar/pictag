<?php

use yii\db\Schema;
use yii\db\Migration;

class m160109_101812_tags extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable ('tag', [

            'id' => 'BIGINT(20) PRIMARY KEY AUTO_INCREMENT',
            'paper_id' => 'BIGINT(20)',
            'user_id' => 'INTEGER',
            'tag_id' => "INTEGER",
            'created_at' => 'INTEGER',
            'updated_at' => 'INTEGER',
        ],  $tableOptions);

        $this->addForeignKey ( 'fk_paper_tag_id', 'tag', 'paper_id', 'paper', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable ('tag');
    }
}
