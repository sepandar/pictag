<?php

use yii\db\Migration;

class m160130_080232_add_user_role extends Migration
{
    public function up()
    {
        $this->addColumn ('user', 'role', 'VARCHAR(50) after status');
    }

    public function down()
    {
        $this->dropColumn ('user', 'role');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
