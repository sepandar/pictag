<?php

use yii\db\Schema;
use yii\db\Migration;

class m160102_121839_add_username extends Migration
{
    public function up()
    {
        $this->addColumn ('user', 'username', 'VARCHAR(50) after email');
    }

    public function down()
    {
        $this->dropColumn ('user', 'username');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
