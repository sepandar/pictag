<?php

use yii\db\Schema;
use yii\db\Migration;

class m160109_191401_paper_extend extends Migration
{
    public function up()
    {
        $this->addColumn('paper', 'type', 'INTEGER(3) after thumbnail');
    }

    public function down()
    {
        $this->dropColumn('paper', 'type');
    }
}
