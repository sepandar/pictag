<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_144901_document extends Migration
{
    public function safeUp()
    {
      $tableOptions = null;

      if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable ('document', [

        'id' => 'VARCHAR(15) PRIMARY KEY',
        'assigned_to' => 'INTEGER',
        'created_at' => 'INTEGER',
        'updated_at' => 'INTEGER',
      ],  $tableOptions);

      //$this->addForeignKey ( 'fk_document_paper_id', 'document', 'assigned_to', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
      $this->dropTable ('document');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
