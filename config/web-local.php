<?php
$config = [

    //'vendorPath' => '/var/www/html/janus/core/app/vendor',
    'components' => [
      'db'         => [
          'class'    => 'yii\db\Connection',
          'dsn'      => 'mysql:host=localhost;dbname=pictag',
          'username' => 'root',
          'password' => '',
          'charset'  => 'utf8',
          'enableQueryCache' => true,
          'enableSchemaCache' => true,
          'schemaCacheDuration' => 86400
      ],

      'assetManager' => [
          'appendTimestamp' => false,
          'linkAssets' => true
      ],
    ]
];

return $config;
