<?php

$config = [
    'id' => 'DigitalArchive',
    'basePath' => dirname(__DIR__),
    //'bootstrap' => ['log'],
    'vendorPath' => dirname(__DIR__) . '/vendor',
    'components' => [

        'redis' => [
            'class' => 'yii\redis\Connection',
            'unixSocket' => '/var/run/redis/redis.sock'
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],
        'jdate' => [
            'class' => 'jDate\DateTime'
        ],
        'request' => [
            'cookieValidationKey' => 'u4wD2HTj1YVL12dYyfbWfK4kUElXjAk1',
        ],
        'cache'      => [
            'class' => 'yii\redis\Cache',
        ],
        'session' => [
            'class' => 'yii\redis\Session'
        ],

        'user' => [
            'identityClass'   => 'janus\user\models\User',
            'loginUrl' => 'login',
            'enableAutoLogin' => true,
            'enableSession' => true
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db'         => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'mysql:host=localhost;dbname=fava_digital_archive',
            'username' => 'fava_user',
            'password' => 'sep123$%^',
            'charset'  => 'utf8',
            'enableQueryCache' => true,
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 86400
        ],

        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'janus\i18n\models\DbMessageSource',
                    'enableCaching' => true,
                    'forceTranslation' => true,
                    'db' => 'db' // replace with your database component if differents
                ],
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => false
        ],
        'view'       => [
            'theme' => [
                'pathMap' => [
                    '@app/views'        => '@app/themes/fava/views'
                ]
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                '<controller:\w+>/<id:\d+>'                  => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'     => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'              => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                'login' => 'user/account/login',
                'register' => 'user/account/register'
            ]
        ]
    ],
    'modules' => [
        'api' => [
          'class' => 'app\modules\api\Module'
        ],
        'admin' => [
          'class' => 'janus\admin\Module',
          'menu' => [
              'modules' => [ 'admin', 'user', 'translate' ]
          ]
        ],
        'user' => [
          'class' => 'janus\user\Module',
          'modelMap' => [
            'User' => 'app\modules\api\models\User',
            'LoginForm' => 'app\modules\api\models\LoginForm'
          ]
        ]
    ]
];

/*
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}
*/

return $config;
