<?php

/* set janus packages path */
Yii::setAlias('janus', dirname(__DIR__) . '/vendor/janus');

/* set bower packages path */
Yii::setAlias('bower', dirname(__DIR__) . '/vendor/bower');

Yii::setAlias('app', dirname(__DIR__));
